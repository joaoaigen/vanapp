<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>@yield('title') - @yield('sub-title')</title>
    <meta content="Operating 24 hours a day, 7 days a week, providing a comprehensive range of vehicle recovery services for both breakdown and accidents of cars, vans, 4x4s and more!
    Don't hesite in contact us!" name="description">

    <!-- Favicons -->
    <link href="{{ asset('assets/img/icon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="{{ url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,700') }}" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
</head>

<body>

<!-- ======= Header ======= -->
@include('layouts.header')
<!-- #header -->

<!-- ======= Hero Section ======= -->
@include('index.first_section')
<!-- End Hero Section -->

<main id="main">

    <!-- ======= About Section ======= -->
    {{--@include('index.about')--}}
    <!-- End About Section -->

    <!-- ======= Services Section ======= -->
    @include('index.services')
    <!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    @include('index.why_us')
    <!-- End Why Us Section -->

    <!-- ======= Portfolio Section ======= -->
    @include('index.portfolio')
    <!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->
    {{--@include('index.testimonials')--}}
    <!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    {{--@include('index.team')--}}
    <!-- End Team Section -->

    <!-- ======= Clients Section ======= -->
    {{--@include('index.clients')--}}
    <!-- End Clients Section -->

    <!-- ======= Contact Section ======= -->
    @include('index.contact')
    <!-- End Contact Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
@include('layouts.footer')
<!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
<a href="https://api.whatsapp.com/send?phone=4407542455887&text=Hello%2C%20I%20come%20from%20the%20Website.%20Could%20you%20help%20me%3F"
   class="move-right-whatsapp whatsapp-button d-flex align-items-center justify-content-center"
   target="_blank"><i class="bi bi-whatsapp"></i>
</a>
<!-- Vendor JS Files -->
<script src="{{ asset('assets/vendor/purecounter/purecounter.js') }}"></script>
<script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>
