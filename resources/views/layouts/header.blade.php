<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex justify-content-between">

        <div class="logo d-flex justify-content-center">
            <!-- Uncomment below if you prefer to use an text logo -->
            <!-- <h1><a href="index.html">NewBiz</a></h1> -->
            <a class="scrollto d-flex justify-content-between" href="{{ route('home') }}"><img src="{{ asset('assets/img/icon.png') }}" alt="" class="img-fluid"> &emsp;<h4 class="h4 mt-2"><b> 24/7 Vehicle Recovery</b></h4></a>
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                {{--<li><a class="nav-link scrollto" href="#about">About</a></li>--}}
                <li><a class="nav-link scrollto" href="#why-us">Why us?</a></li>
                <li><a class="nav-link scrollto" href="#services">Services</a></li>
                <li><a class="nav-link scrollto " href="#portfolio">Portfolio</a></li>
                {{--<li><a class="nav-link scrollto" href="#team">Team</a></li>--}}
                {{--<li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                    </ul>
                </li>--}}
                <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header>
