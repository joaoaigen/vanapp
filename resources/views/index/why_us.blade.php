<section id="why-us">
    <div class="container" data-aos="fade-up">
        <header class="section-header">
            <h3>Why choose us?</h3>
            <p>A recovery service should be prompt and efficient, ensuring minimum inconvenience to you. This is what you can expect from Van App Recovery. We offer a range of different services across England! Whether you need Roadside Assistance or a Vehicle Recovery Service you can count on, choose Van App Recovery. </p>
        </header>

        <div class="row row-eq-height justify-content-center">

            <div class="col-lg-4 mb-4">
                <div class="card" data-aos="zoom-in" data-aos-delay="100">
                    <i class="bi bi-check-square"></i>
                    <div class="card-body">
                        <h5 class="card-title">An efficient vehicle towing</h5>
                        <p class="card-text">If your vehicle has broken down and cannot be repaired by the roadside then you’ll need a Car Recovery operator to collect your vehicle and transport it to a local garage or auto repair centre.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4">
                <div class="card" data-aos="zoom-in" data-aos-delay="200">
                    <i class="bi bi-person-check"> </i>
                    <div class="card-body">
                        <h5 class="card-title">Trusted by partners</h5>
                        <p class="card-text">We have partners who trust our work, <a href="https://www.leggsvehicles.com">Leggs Vehicles Ltd</a> is our best Garage partner across England. So if you need an exceptional Garage, you know which one to choose! </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
