<section id="portfolio" class="clearfix">
    <div class="container" data-aos="fade-up">

        <header class="section-header">
            <h3 class="section-title">Our Portfolio</h3>
        </header>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
            <div class="col-lg-12">
                {{--<ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">All</li>
                    <li data-filter=".filter-app">App</li>
                    <li data-filter=".filter-card">Card</li>
                    <li data-filter=".filter-web">Web</li>
                </ul>--}}
            </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="assets/img/portfolio/app1.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <div>
                            <a href="assets/img/portfolio/app1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox link-preview"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="assets/img/portfolio/app2.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <div>
                            <a href="assets/img/portfolio/app2.jpg" class="portfolio-lightbox link-preview" data-gallery="portfolioGallery"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="assets/img/portfolio/app3.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <div>
                            <a href="assets/img/portfolio/app3.jpg" class="portfolio-lightbox link-preview" data-gallery="portfolioGallery"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="assets/img/portfolio/app4.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <div>
                            <a href="assets/img/portfolio/app4.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox link-preview"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="assets/img/portfolio/app5.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <div>
                            <a href="assets/img/portfolio/app5.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox link-preview"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <div class="portfolio-wrap">
                    <img src="assets/img/portfolio/app6.jpg" class="img-fluid" alt="">
                    <div class="portfolio-info">
                        <div>
                            <a href="assets/img/portfolio/app6.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox link-preview"><i class="bi bi-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
