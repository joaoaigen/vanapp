<section id="services" class="section-bg">
    <div class="container" data-aos="fade-up">

        <header class="section-header">
            <h3>Services</h3>
            <p>Check out our services below, quick response times, great customer service and affordable prices.
                Our team will help get you back on the road as quickly as possible.</p>
        </header>

        <div class="row justify-content-center">

            <div class="col-md-6 col-lg-5" data-aos="zoom-in" data-aos-delay="100">
                <div class="box">
                    <div class="icon"><i class="bi bi-truck-flatbed" style="color: #ff689b;"></i></div>
                    <h4 class="title"><a href="">Breakdown Recovery</a></h4>
                    <p class="description">The Van App team are all fully trained in all aspects of accident recovery, and are able to offer a comprehensive solution to meet all your recovery needs. If you want to know more about our vehicle recovery services or need a quote, please <a>get in touch</a> and our team will be happy to help. </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-5" data-aos="zoom-in" data-aos-delay="200">
                <div class="box">
                    <div class="icon"><i class="bi bi-clock-history" style="color: #e9bf06;"></i></div>
                    <h4 class="title"><a href="">24Hr Recovery</a></h4>
                    <p class="description">If you need recovery assistance, contact us on our 24 hour recovery number: Tel: +44 07577 408352 <br>Whether you need an urgent breakdown recovery or a regular service for your car, van or truck, we are glad to help you!<br><br></p>
                </div>
            </div>

            <div class="col-md-6 col-lg-5" data-aos="zoom-in" data-aos-delay="100">
                <div class="box">
                    <div class="icon"><i class="bi bi-cone-striped" style="color: #3fcdc7;"></i></div>
                    <h4 class="title"><a href="">Roadside Breakdown Recovery</a></h4>
                    <p class="description">We know that you want to get back on the road and complete your journey. Part of our breakdown service is to assist at the roadside wherever possible with jump starts, wheel changes etc. Anything more serious and we can transport your vehicle back to our garage partner <a href="https://leggsvehicles.com">Leggs Vehicles Ltd</a> or to a destination of your choice.

                        If we can't fix them on the roadside, we'll get you back to our garage and carry out the repair work from there.</p>
                </div>
            </div>

        </div>

    </div>
</section>
