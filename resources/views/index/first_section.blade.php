<section id="hero" class="clearfix">
    <div class="container" data-aos="fade-up">

        <div class="hero-img" data-aos="zoom-out" data-aos-delay="200">
            <img src="{{ asset('assets/img/portfolio/app4.jpg') }}" alt="" class="img-fluid rounded rounded-3 shadow-lg shadow-sm shadow">
        </div>

        <div class="hero-info" data-aos="zoom-in" data-aos-delay="100">
            <h2><i>24/7 accident and breakdown recovery <br>for cars and vans. <br>Trusted and outstanding recovery service.</i></h2>
            <div>
                <a target="_blank" href="https://api.whatsapp.com/send?phone=4407542455887&text=Hello%2C%20I%20come%20from%20the%20Website.%20Could%20you%20help%20me%3F" class="btn-get-started scrollto" style="background-color: #25d366"><div class="icon"><i class="bi bi-whatsapp"></i> Contact us</div></a>
                <a href="#services" class="btn-services scrollto">Our Services</a>
            </div>
        </div>

    </div>
</section>
