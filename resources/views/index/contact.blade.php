<section id="contact">
    <div class="container-fluid" data-aos="fade-up">

        <div class="section-header">
            <h3>Contact Us</h3>
        </div>

        <div class="row d-flex align-content-center justify-content-lg-center">

            <div class="col-lg-6">
                <div class="row">
                    <div class="col-md-4 info">
                        <i class="bi bi-envelope"></i>
                        <p>contact@vanapp.co.uk</p>
                    </div>
                    <div class="col-md-4 info">
                        <i class="bi bi-phone"></i>
                        <p>+44 07542 455887</p>
                    </div>
                </div>

                <div class="form">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                            </div>
                            <div class="form-group col-lg-6 mt-3 mt-lg-0">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</section>
